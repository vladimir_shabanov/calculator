package com.example.vladimirshabanov.calculator;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.javia.arity.Symbols;
import org.javia.arity.SyntaxException;

public class MainActivity extends Activity {

    private TextView mTextView;
    private String result = "";

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;


    @Override
    public void onResume() {
        super.onResume();
        // Add the following line to register the Session Manager Listener onResume
        mSensorManager.registerListener(mShakeDetector, mAccelerometer,	SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        // Add the following line to unregister the Sensor Manager onPause
        mSensorManager.unregisterListener(mShakeDetector);
        super.onPause();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        Display display = ((WindowManager) this.getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int rotation = display.getRotation();

        setContentView(R.layout.activity_main);
        mTextView = (TextView) findViewById(R.id.exprText);

        // ShakeDetector initialization
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(int count) {
                eval();
            }
        });

        Button buttonClr = (Button) findViewById(R.id.buttonClr);

        buttonClr.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move);
                mTextView.startAnimation(animation);

                mTextView.setText("");
                return false;
            }
        });

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mTextView.setText(savedInstanceState.getCharSequence("EXPR"));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putCharSequence("EXPR", mTextView.getText());
    }

    public void eval() {
        try {
            Symbols symbols = new Symbols();
            double value = symbols.eval(mTextView.getText().toString());
            if ((value - (int)value) == 0.0)
                mTextView.setText(Integer.toString((int)value));
            else
                mTextView.setText(Double.toString(value));
            Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move);
            mTextView.startAnimation(animation1);

            result = mTextView.getText().toString();

        } catch (SyntaxException e) {
            Context context = getApplicationContext();
            Toast.makeText(context, "Incorrect expression", Toast.LENGTH_SHORT).show();

        }
    }


    public void onClick(View view) throws SyntaxException {
        Animation animation = new AlphaAnimation(1.0f, 0.5f);
        animation.setDuration(100);
        view.startAnimation(animation);

        switch (view.getId()) {
            case R.id.buttonOne:
                mTextView.append("1");
                break;
            case R.id.buttonTwo:
                mTextView.append("2");
                break;
            case R.id.buttonThree:
                mTextView.append("3");
                break;
            case R.id.buttonFour:
                mTextView.append("4");
                break;
            case R.id.buttonFive:
                mTextView.append("5");
                break;
            case R.id.buttonSix:
                mTextView.append("6");
                break;
            case R.id.buttonSeven:
                mTextView.append("7");
                break;
            case R.id.buttonEight:
                mTextView.append("8");
                break;
            case R.id.buttonNine:
                mTextView.append("9");
                break;
            case R.id.buttonNull:
                mTextView.append("0");
                break;
            case R.id.buttonLbr:
                mTextView.append("(");
                break;
            case R.id.buttonRbr:
                mTextView.append(")");
                break;
            case R.id.buttonPlus:
                mTextView.append("+");
                break;
            case R.id.buttonMinus:
                mTextView.append("-");
                break;
            case R.id.buttonMul:
                mTextView.append("*");
                break;
            case R.id.buttonDiv:
                mTextView.append("/");
                break;
            case R.id.buttonPow:
                mTextView.append("^");
                break;
            case R.id.buttonDot:
                mTextView.append(".");
                break;
            case R.id.buttonE:
                mTextView.append("e");
                break;
            case R.id.buttonSin:
                mTextView.append("sin(");
                break;
            case R.id.buttonCos:
                mTextView.append("cos(");
                break;
            case R.id.buttonTg:
                mTextView.append("tg(");
                break;
            case R.id.buttonCtg:
                mTextView.append("ctg(");
                break;
            case R.id.buttonEval:
                eval();
                break;
            case R.id.buttonClr:
                if (mTextView.getText().toString().toUpperCase().equals("INFINITY") ||
                        mTextView.getText().toString().toUpperCase().equals("NAN") ||
                        mTextView.getText().toString().equals(result)) {
                    mTextView.setText("");
                    result = "";
                }
                if (mTextView.getText().length() > 0)
                    mTextView.setText(mTextView.getText().subSequence(0, mTextView.getText().length() - 1));

                break;
        }

        if (!mTextView.getText().toString().equals(result)) {
            ((Button)findViewById(R.id.buttonClr)).setText("DEL");
        } else {
            ((Button)findViewById(R.id.buttonClr)).setText("CLEAR");
        }
    }
}
